Ansible and AWS Talk Code
=========================

This repository was created to support a talk given at the Brisbane DevOps
group, on February 27th, 2014, about using Ansible with AWS. You can see the
talk slides here:

http://www.slideshare.net/rmcleay/devops-in-a-regulated-world-aka-ansible-aws-and-jenkins

In this repository, there is some sample Ansible code to deploy:

- A VPC and associated security groups
- Two t1.micro instances
- An elastic load balancer
- A db.t1.micro postgresql database (multi-AZ).

Note that due to current limitations in Ansible, the elastic load balancer
cannot be created within the VPC. Therefore, adding the VPC instances to the
ELB will fail. In other words, feel free to read and reuse, but running this
as-is will result in errors.

Pre-requisites
--------------

In order to run this (in addition to Ansible), you will need to have boto
(https://code.google.com/p/boto/) installed, and either set the relevant IAM environment
variables or write a $HOME/.boto configuration file.

Actually Running The Playbook
-----------------------------

Firstly, note that running this will cost you money (even if you're eligible
for the free tier). Secondly, it will end in an error. That said, you can run it via:

	ansible-playbook -i server_inventory site.yml
